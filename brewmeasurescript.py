#!/usr/bin/python
import os
import time
import RPi.GPIO as GPIO
import sys
import time

pouredML = 0.0

# no idea why this needs a channel argument
def my_callback(channel):
  # 2.2 ml poured; handle it how you like
  # print "falling edge detected on 22"
  global pouredML
  pouredML += 2.2


def main():

  boardRevision = GPIO.RPI_REVISION
  GPIO.setmode(GPIO.BCM) # use real GPIO numbering
  GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)


  # no idea what bouncetime needs to be, we will have to experiment
  # but it needs to be faster than the max frequency of the sensor data
  # 10 ms, 50% on = 50 Hz, 1800 RPM, that's probably good enough
  GPIO.add_event_detect(22, GPIO.FALLING, callback=my_callback, bouncetime=10)
  while True:
    time.sleep(1)
    print pouredML
main()
