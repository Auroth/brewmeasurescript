#!/usr/bin/python
import os
import time
import math
import logging
import pygame
import sys
from pygame.locals import *
import RPi.GPIO as GPIO
from flowmeter import *

# boardRevision = GPIO.RPI_REVISION
GPIO.setmode(GPIO.BCM)  # use real GPIO numbering
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(23, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# set up pygame
pygame.init()

#variable shit
f = open('porn archive', 'r')
meter1Value = f.readline()
meter1Value = float(meter1Value)
meter2Value = f.readline()
meter2Value = float(meter2Value)
f.close()

# set up the window
VIEW_WIDTH = 1600
VIEW_HEIGHT = 900
pygame.display.set_caption('KEGBOT')
view_mode = 'normal'

# hide the mouse
pygame.mouse.set_visible(False)

# set up the flow meters
fm = FlowMeter('metric', ["beer"], meter1Value)
fm2 = FlowMeter('metric', ["root beer"], meter2Value)
# set up the colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# set up the window surface
windowSurface = pygame.display.set_mode(
    (VIEW_WIDTH, VIEW_HEIGHT), FULLSCREEN, 32)
windowInfo = pygame.display.Info()
FONTSIZE = 48
LINEHEIGHT = 28
basicFont = pygame.font.SysFont(None, FONTSIZE)

# set up the backgrounds
bg = pygame.image.load('pooh-bear.jpg')

# draw some text into an area of a surface
# automatically wraps words
# returns any text that didn't get blitted


def drawText(surface, text, color, rect, font, aa=False, bkg=None):
    rect = Rect(rect)
    y = rect.top
    lineSpacing = -2

    # get the height of the font
    fontHeight = font.size("Tg")[1]

    while text:
        i = 1

        # determine if the row of text will be outside our area
        if y + fontHeight > rect.bottom:
            break

        # determine maximum width of line
        while font.size(text[:i])[0] < rect.width and i < len(text):
            i += 1

        # if we've wrapped the text, then adjust the wrap to the last word
        if i < len(text):
            i = text.rfind(" ", 0, i) + 1

        # render the line and blit it to the surface
        if bkg:
            image = font.render(text[:i], 1, color, bkg)
            image.set_colorkey(bkg)
        else:
            image = font.render(text[:i], aa, color)

        surface.blit(image, (rect.left, y))
        y += fontHeight + lineSpacing

        # remove the text we just blitted
        text = text[i:]

    return text


def renderThings(flowMeter, flowMeter2, windowSurface, basicFont):
    # Clear the screen
    windowSurface.blit(bg, (0, 0))

    # Draw Ammt Poured
    text = basicFont.render("CURRENT", True, WHITE, BLACK)
    textRect = text.get_rect()
    windowSurface.blit(text, (40, 20))
    if fm.enabled:
        text = basicFont.render(fm.getFormattedThisPour(), True, WHITE, BLACK)
        textRect = text.get_rect()
        windowSurface.blit(text, (40, 30 + LINEHEIGHT))
    if fm2.enabled:
        text = basicFont.render(fm2.getFormattedThisPour(), True, WHITE, BLACK)
        textRect = text.get_rect()
        windowSurface.blit(text, (40, 30 + (2 * (LINEHEIGHT + 5))))

    # Draw Ammt Poured Total
    text = basicFont.render("TOTAL", True, WHITE, BLACK)
    textRect = text.get_rect()
    windowSurface.blit(text, (windowInfo.current_w - textRect.width - 40, 20))
    if fm.enabled:
        text = basicFont.render(fm.getFormattedTotalPour(), True, WHITE, BLACK)
        textRect = text.get_rect()
        windowSurface.blit(
            text, (windowInfo.current_w - textRect.width - 40, 30 + LINEHEIGHT))
    if fm2.enabled:
        text = basicFont.render(
            fm2.getFormattedTotalPour(), True, WHITE, BLACK)
        textRect = text.get_rect()
        windowSurface.blit(
            text, (windowInfo.current_w - textRect.width - 40, 30 + (2 * (LINEHEIGHT + 5))))

    # Display everything
    pygame.display.flip()

# Beer, on Pin 23.
def doAClick(channel):
    currentTime = int(time.time() * FlowMeter.MS_IN_A_SECOND)
    if fm.enabled == True:
        fm.update(currentTime)

# Root Beer, on Pin 24.
def doAClick2(channel):
    currentTime = int(time.time() * FlowMeter.MS_IN_A_SECOND)
    if fm2.enabled == True:
        fm2.update(currentTime)

GPIO.add_event_detect(
    22, GPIO.RISING, callback=doAClick, bouncetime=10)  # Beer on Pin 23
GPIO.add_event_detect(
    23, GPIO.RISING, callback=doAClick2, bouncetime=10)  # Root Beer on Pin 24

# main loop
while True:
    # Handle keyboard events
    for event in pygame.event.get():
        if event.type == QUIT or (event.type == KEYUP and event.key == K_ESCAPE):
            GPIO.cleanup()
            pygame.quit()
            sys.exit()
        elif event.type == KEYUP and event.key == K_1:
            fm.enabled = not fm.enabled
        elif event.type == KEYUP and event.key == K_2:
            fm2.enabled = not fm2.enabled
        elif event.type == KEYUP and event.key == K_9:
            fm.clear()
        elif event.type == KEYUP and event.key == K_0:
            fm2.clear()

    currentTime = int(time.time() * FlowMeter.MS_IN_A_SECOND)

    if (currentTime % 5 == 0):
        if (fm.getChanged() == True) or (fm2.getChanged() == True):
            p = open('porn archive', 'w')
            p.write(fm.getTotalPour() + '\n')
            p.write(fm2.getTotalPour() + '\n')
            fm.clearChanged()
            fm2.clearChanged()
            p.close()

    view_mode = 'normal'

    # reset flow meter after each pour (2 secs of inactivity)
    if (fm.thisPour <= 0.23 and currentTime - fm.lastClick > 2000):
        fm.thisPour = 0.0

    if (fm2.thisPour <= 0.23 and currentTime - fm2.lastClick > 2000):
        fm2.thisPour = 0.0

    # Update the screen
    renderThings(fm, fm2, windowSurface, basicFont)
